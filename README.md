ARM11 Assembler and Emulator

- An ARM emulator that simulates the execution of an ARM binary file on a Raspberry Pi, that contains a Broadcom BCM2835 SoC, with an ARM1176JZF-S 700MHz processor.

- An ARM assembler that translates an ARM assembly source file into a binary file that can be executed by the emulator

Demo

- A simple ARM assembly program that flashes an LED on a Raspberry Pi via the GPIO pins

- The classic Tower Stack game implemented as an ARM assembly program that flashes an array of LEDs, and takes input from a button on a Raspberry Pi via the GPIO pins
